import React, { useState } from 'react';

const Counter = () => {
    //This is just a sample using generics [s]
    type Information = { name: string; description: string };
    const [information, setInformation] = useState<Information | null>(null);
    //This is just a sample using generics [e]


    const [count, setCount] = useState<number>(0);
    const onIncrease = () => setCount(count + 1);
    const onDecrease = () => setCount(count - 1);

    return (
        <div>
            <h1>{count}</h1>
            <div>
                <button onClick={onIncrease}>+1</button>
                <button onClick={onDecrease}>-1</button>
            </div>
        </div>
    );
}

export default Counter;