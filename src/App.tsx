import React from 'react';
import logo from './logo.svg';
import './App.css';
import './exercise/Greeting';
import Greetings from './exercise/Greeting';
import Counter from './exercise/Counter';
import CounterReducer from './exercise/CounterReducer';
import MyForm from './exercise/MyForm';
import ReducerSample from './exercise/ReducerSample';
import ReducerSample2 from './exercise/ReducerSample2';
import { SampleProvider } from './exercise/SampleContext';

const App: React.FC = () => {
    const onClick = (name: string) => {
        console.log(`${name} says hello`);
    }

    const onSubmit = (form: { name: string; description: string }) => {
        console.log(form);
    };

    return (
        <div className="App">
            {/*<header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>*/}
            <div>
                <Greetings name='test-name' mark='test-mark' optional='test-optional' onClick={onClick} />
            </div>
            <hr />
            <div>
                <Counter />
            </div>
            <hr />
            <div>
                <CounterReducer />
            </div>
            <hr />
            <div>
                <MyForm onSubmit={onSubmit} />
            </div>
            <hr />
            <div>
                <ReducerSample />
            </div>
            <hr />
            <div>
                <SampleProvider>
                    <ReducerSample2 />
                </SampleProvider>
            </div>
        </div>
    );
}

export default App;
